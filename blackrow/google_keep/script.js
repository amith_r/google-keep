// set this global variable true, when the sidebar is visible.
var SIDEBAR_ACTIVE = false;
var id = null;
var ONCLICK_VALUE = false;

//To disable and enable input to do field.
function enableHideDiv(){
	document.getElementById("input-div") .style.display = "none";
	document.getElementById("input-hide-div").style.display = "block";
	document.getElementById("title-div").focus();
}
function disableHideDiv(){
	if(!document.getElementById("note-div").innerHTML){
        document.getElementById("input-div") .style.display = "block";
        document.getElementById("input-hide-div") .style.display = "none";
	}else{

	}
}    

//to check wether sidebar transition is over or not.
var element = document.getElementById("side-menu");
element.addEventListener("transitionend",function(event){
    if(event.propertyName){
        if(!SIDEBAR_ACTIVE)
            SIDEBAR_ACTIVE = true;
    }},true);

//To check whether child onde or not.
function isChildOf(parentNode, childNode) {
	if('contains' in parentNode) {
		return parentNode.contains(childNode);
	}
	else {
		return parentNode.compareDocumentPosition(childNode) % 16;
	}
}

//To hide side bar menu
function hideSidebar(e) {
    var isInsideSideMenu = isChildOf(document.getElementById("side-menu"),e.target);
    if (SIDEBAR_ACTIVE && !isInsideSideMenu){
        document.getElementById("side-menu").style.transform = "translate(-240px,0px)";   
        window.setTimeout(function(){
            window.location = "/google_keep/index.html";
        },500);
        
         SIDEBAR_ACTIVE = false;
    }
}

//This function replace an element with a dummy element.
function replaceWithDummy(element) {
	var dummy = document.createElement("div");
	dummy.id = "dummy_container";
	dummy.className = "note-container";
	//TO DO - Do the following by CSS class. 
	dummy.style.visibility = "hidden";
	dummy.style.height = "100px";
	//Replacing dummmy with element.
	element.parentNode.replaceChild(dummy,element);
}
function showDivAtCenter(element){
	/* Approch 1
	 * When a user clicks on the note container we read the top and left position of the container and
	 * make it an absolute positoned element. then set the position to the center of the page. */
	var top = element.getBoundingClientRect().top;
	var left = element.getBoundingClientRect().left;
	/* make the element absolute and create a dummy element in its position */
	replaceWithDummy(element);
	element.classList.add("no-transition");
	// Now attach the element to the overlay
	var overlay = document.getElementById("overlay");
    var tick =document.getElementById("icon-ok");
	overlay.appendChild(element);
	element.style.position = "absolute";
	element.style.top = top+"px";
	element.style.left = left+"px";
	element.style.zIndex = 1100;
	document.getElementById("overlay").style.visibility = "visible";
	document.getElementById("overlay").style.opacity = 1;
	//id = element.id;
	element.classList.remove("no-transition");
	// Add a timeout so that the DOM gets ready before animating stuff
	setTimeout(function(){
		// Now Change the position, width and height of the container
		element.classList.add("popup-center");
	},10);
	console.log(element.childNodes);
    element.setAttribute("onclick",null);
    element.childNodes[3].contentEditable = 'true';
    console.log(element);
}

function hideNoteContainer(event){
	event.preventDefault();
    var container = document.querySelectorAll("#overlay .note-container")[0];
    var dummy =document.getElementById("dummy_container");
    var overlay = document.getElementById("overlay");
    // remove container from overlay.
    container.setAttribute("style",null);
    container.classList.remove("popup-center");
    document.getElementById("overlay").style.opacity = 0;
    setTimeout(function(){ 
		document.getElementById("overlay").style.visibility = "hidden";
	},300);
    dummy.parentNode.replaceChild(container,dummy);
    //var element = document.getElementById(id);
    //element.classList.remove("display-new-div");
    container.setAttribute("onclick","showDivAtCenter(this)");
   container.childNodes[3].contentEditable = 'false';
}
