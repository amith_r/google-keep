// set this global variable true, when the sidebar is visible.
var SIDEBAR_ACTIVE = false;
var HIDEBAR_DIV = false;
var ONCLICK_VALUE = false;
var nodeArray = [];
var section = document.getElementById("section");
var tickedValueOfSelectedNote = false;

//To disable and enable input to do field.
function enableHideDiv() {
    event.stopPropagation();
    HIDEBAR_DIV = true;
	document.getElementById("input-div").style.display = "none";
	document.getElementById("input-hide-div").style.display = "block";
	document.getElementById("title-div").focus();
}

//to check wether sidebar transition is over or not.
var element = document.getElementById("side-menu");
element.addEventListener("transitionend", function (event) {
    if (event.propertyName) {
        if (!SIDEBAR_ACTIVE) SIDEBAR_ACTIVE = true;
    } },true);

//To check whether child onde or not.
function isChildOf(parentNode, childNode) {
	if('contains' in parentNode) {
		return parentNode.contains(childNode);
	}
	else {
		return parentNode.compareDocumentPosition(childNode) % 16;
	}
}

//To hide side bar menu
function hideContainer(e) {
    //console.log(e.target);
    var contentInputDiv = document.getElementById("note-div").innerHTML;
    var contentTitleDiv = document.getElementById("title-div").innerHTML;
    var isInsideSideMenu = isChildOf(document.getElementById("side-menu"),e.target);
    var isInsideInputDiv = isChildOf(document.getElementById("input-hide-div"),e.target);
    if (SIDEBAR_ACTIVE && !isInsideSideMenu){
        document.getElementById("side-menu").style.transform = "translate(-240px,0px)";   
        window.setTimeout(function(){
            window.location = "/google_keep/index.html";
        },500);
        SIDEBAR_ACTIVE = false;
    }
    else if(HIDEBAR_DIV && !isInsideInputDiv  && (contentInputDiv =="") && (contentTitleDiv =="")) {
        hideInputDiv();
    }
   
}

/*This function trigers when user clicks 
 *outside the input container or when he press done button after entering  note*/
function hideInputDiv () {
     document.getElementById("title-div").setAttribute("data-placeholder","Title");    
     document.getElementById("note-div").setAttribute("data-placeholder","Add Note");
     document.getElementById("input-div").style.display = "block";
     document.getElementById("input-hide-div").style.display = "none";
     HIDEBAR_DIV = false;
}

//This function replace an element with a dummy element.
function replaceWithDummy(element) {
	var dummy = document.createElement("div");
	dummy.id = "dummy_container";
	dummy.className = "note-container";
	//TO DO - Do the following by CSS class. 
	dummy.style.visibility = "hidden";
	dummy.style.height = "100px";
	//Replacing dummmy with element.
	element.parentNode.replaceChild(dummy,element);
}
function showDivAtCenter(element){
    //if ticked element remove its tick.
    var itemTicked = ticked[element.id.split('-')[1]];
    if(itemTicked == true){
        tickedValueOfSelectedNote = true; 
        element.classList.remove("select-container");
        itemTicked = undefined;
    }
	/* Approch 1
	 * When a user clicks on the note container we read the top and left position of the container and
	 * make it an absolute positoned element. then set the position to the center of the page. */
	var top = element.getBoundingClientRect().top;
	var left = element.getBoundingClientRect().left;
	/* make the element absolute and create a dummy element in its position */
	replaceWithDummy(element);
	element.classList.add("no-transition");
    element.classList.add("display-bottom-bar");
	// Now attach the element to the overlay
	var overlay = document.getElementById("overlay");
    var divDone = document.createElement("div");
    divDone.id = "done_button";
    divDone.innerHTML = "Done";
    divDone.style.float = "right";
    divDone.setAttribute("onclick","hideNoteContainer(event)");
    element.childNodes[4].appendChild(divDone);
	overlay.appendChild(element);
	element.style.position = "relative";
	element.style.top = top+"px";
	element.style.left = left+"px";
	element.style.zIndex = 1100;
	document.getElementById("overlay").style.visibility = "visible";
	document.getElementById("overlay").style.opacity = 1;
	element.classList.remove("no-transition");
	// Add a timeout so that the DOM gets ready before animating stuff
	setTimeout(function(){
		// Now Change the position, width and height of the container
		element.classList.add("popup-center");
	},10);
    element.setAttribute("onclick",null);
    element.childNodes[2].contentEditable = 'true';
    element.childNodes[3].contentEditable = 'true';
}

//Used to hide the  ocntainer.
function hideNoteContainer(event){
   if(event.target.id != "done_button"){
        event.preventDefault();
   }
    var container = document.querySelectorAll("#overlay .note-container")[0];
    var dummy =document.getElementById("dummy_container");
    var overlay = document.getElementById("overlay");

    container.classList.remove("display-bottom-bar");
   //  remove container from overlay.
    container.setAttribute("style",null);
    container.classList.remove("popup-center");
    document.getElementById("overlay").style.opacity = 0;
    setTimeout(function(){ 
		document.getElementById("overlay").style.visibility = "hidden";
	},300);
    dummy.parentNode.replaceChild(container,dummy);
   container.setAttribute("onclick","showDivAtCenter(this)");
   container.childNodes[2].contentEditable = 'false';
   container.childNodes[3].contentEditable = 'false';
    var doneButton = document.getElementById("done_button");
    doneButton.parentNode.removeChild(doneButton);
    updateNoteInLocalStorage(container.childNodes[2].innerHTML,container.childNodes[3].innerHTML,container.id);
    if(event.target.id == "done_button"){
        event.stopPropagation();
    }
    //used if the note was ticked when opening so it should reset to ticked state.
    if(tickedValueOfSelectedNote == true){
        ticked[container.id.split('-')[1]] = true;
        tickedValueOfSelectedNote = false; 
        container.classList.add("select-container");
    }

}

//used to  remove the data place holder when key pressed in input div. 
function keyPressed(element){
    element.setAttribute("data-placeholder","");
}


