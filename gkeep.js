var nodeArray = [];
var nodeIdCounter = 0;
var section = document.getElementById("section");
var CREATE_DIV = false;
var ticked = new Array(50);

//General properties of Note-container.
function generalNoteContainerFeatures(){
	this.tickIconInnerHtml = "<div class='tick-icon'><span class='i-name icon-ok-circled' id='icon-ok' onclick = 'selectContainer()'></span></div>"+
							 "<div class='close-icon'><span class='i-name icon-cancel-circled id='icon-close' onclick = 'closeContainer()'></span></div>";								
	this.bottomNoteInnerHtml = "<div class ='note-bottom-option'>"+
				                "<div class='o'><span class='i-name icon-up-hand' id='icon-remind'></span></div>"+			
				    			"<div class='o'><span class='i-name icon-user-add' id='icon-add'></span></div>"+
				    			"<div class='o' id = 'select-color'><span class='i-name icon-art-gallery' id = 'icon-gal'></span></div>"+
				    			"<div class = 'color-picker'>"+ 
				    			"<span class='i-name icon-art-gallery color-icon' id='icon-gal1' onclick = 'changeColor()'></span>"+
				    			"<span class='i-name icon-art-gallery color-icon' id='icon-gal2' onclick = 'changeColor()'></span><span class='i-name icon-art-gallery color-icon' id='icon-gal3' onclick = 'changeColor()'></span>"+
				    			"<br><span class='i-name icon-art-gallery color-icon' id='icon-gal4' onclick = 'changeColor()'></span><span class='i-name icon-art-gallery color-icon' id='icon-gal5' onclick = 'changeColor()'></span>"+
				    			"<span class='i-name icon-art-gallery color-icon' id='icon-gal6' onclick = 'changeColor()'></span>"+
				    			"</div>"+
				                "<div class='o'><span class='i-name icon-download-outline' id='icon-gal'></span></div>"+
				                "<div class='o'><span class='i-name  icon-ellipsis-vert' id='icon-gal' onclick = 'showOptionMenu()'></span></div>"+
				                "</div>";
}

//Unique properties of Note-container.
function uniqueNoteContainerFeatures(id){
	this.id = id;
	this.head = "";
	this.notes = "";
} 
//Inheriting General properties from parent.
uniqueNoteContainerFeatures.prototype = new generalNoteContainerFeatures;

uniqueNoteContainerFeatures.prototype.getNote = function(){
		return this.notes;
	}

uniqueNoteContainerFeatures.prototype.setNote = function(uniqueNote){
		this.notes = uniqueNote;
	}


//Loaded when document is ready.Used to create default Note containers.
function inovkeExisitingDivGenerator(){
	//Check initiall nodes and dispaly the
	if(JSON.parse(localStorage.getItem("tempValue")) == null){
		setItemsToLocalStorage("tempValue",0);
	}
	var x = JSON.parse(localStorage.getItem("tempValue"));
	if( x == 0){
		for(var k = 1;k <= 4;k++ ){
		switch(k){
			case 1:var firstContainer = new uniqueNoteContainerFeatures(++nodeIdCounter);
					firstContainer.notes ="Done with a note?"+ 
					"Use the archive button or swipe it away on Android."+
					"<br><br>Give it a try! You can always search for it later.";
					flatStringify(firstContainer);
					nodeArray.push(firstContainer);
					break;
			case 2:var secondContainer = new uniqueNoteContainerFeatures(++nodeIdCounter);
				   secondContainer.notes = "<br>Capture what’s on your mind.<br><br>"+
					              			"Add notes, lists, photos, and audio to Keep."
					              			flatStringify(secondContainer);
					              			nodeArray.push(secondContainer);	
					              			break;
			case 3:var thirdContainer = new uniqueNoteContainerFeatures(++nodeIdCounter);
				   thirdContainer.notes = "Make A List"
				   						   flatStringify(thirdContainer);
										   nodeArray.push(thirdContainer);	
										   break;
			case 4:var fourthContainer = new uniqueNoteContainerFeatures(++nodeIdCounter);
			       fourthContainer.notes = "Any way you access Keep, all your notes stay in sync."+
								            "<br><br>On the web<br>"+
								            "<a href ='https://keep.google.com' target ='_blank'>"+
								            "https://keep.google.com</a>"+
								            "<br><br> On Android <br>"+
								            "<a href='https://g.co/keep' target = "+
								            "'_blank'>https://g.co/keep</a>"+
								            "<br><br> In Chrome<br>"+
								            "<a href ='https://g.co/keepinchrome'>https://g.co/keepinchrome</a>";
								            flatStringify(fourthContainer);
											nodeArray.push(fourthContainer);
											break;
		}
	}
	localStorage.setItem("tempValue",1);
	setItemsToLocalStorage("Note_Objects",nodeArray);
	}
	fetchFromLocalStorage("Note_Objects");	
}

//This will display existing nodes. 
function displayNotes(node){
		var imageSelected = document.getElementById("inputFileToLoad").files;
		var containerDiv = document.createElement("div");
		containerDiv.setAttribute("class","note-container");
		containerDiv.id = "container-"+node.id;
		containerDiv.setAttribute("onclick","showDivAtCenter(this)");
		containerDiv.classList.add("ui-sortable-handle");  	
		containerDiv.innerHTML = node.tickIconInnerHtml;
		section.appendChild(containerDiv);
		var titleDiv = document.createElement("div");
		titleDiv.id = "title-"+node.id;
		titleDiv.setAttribute("class","title");
		titleDiv.innerHTML = node.head;
		var noteDiv = document.createElement("div");
		noteDiv.id = "notes-"+node.id;
		noteDiv.setAttribute("class","notes");
		noteDiv.innerHTML = node.notes;
		//adding image inside note div 
		if(imageSelected.length > 0){
	    	var imageToLoad = imageSelected[0];
	    	if(imageToLoad.type.match("image.*")){
				var imageReader = new FileReader();
				var image = document.createElement("img");
	    		image.setAttribute("class","img");
	    		imageReader.onload = function(fileLoadedEvent){
	    			console.log(fileLoadedEvent.target);
	    			image.src = fileLoadedEvent.target.result;
	    			document.getElementById(noteDiv.id).appendChild(image);
	    		}
	    		imageReader.readAsDataURL(imageToLoad);
	    	}
	    }
		if(node.id > 4){
			titleDiv.classList.add("newDivs-title");
			noteDiv.classList.add("newDivs");
		}
		if(node.id > 4 && (CREATE_DIV == true)){
			var result = JSON.parse(localStorage.getItem("Note_Objects")); 
	        for(i=0;i<result.length;i++);
	   		var item = result[i];
			flatStringify(node);
			result[i] = node;
			localStorage.setItem("Note_Objects",JSON.stringify(result));
			}
			containerDiv.appendChild(titleDiv);
			containerDiv.appendChild(noteDiv);
			containerDiv.innerHTML = containerDiv.innerHTML +
									node.bottomNoteInnerHtml;
		    CREATE_DIV = false;
	     			
}

//used to update note in each container in LocalStorage.
function updateNoteInLocalStorage(updateTitle,updateNote,id){
	console.log(updateTitle+"and"+updateNote);
		var y = JSON.parse(localStorage.getItem("tempValue"));
		localStorage.setItem("tempValue",++y);
		var arrayVal = JSON.parse(localStorage.getItem("Note_Objects")) 
		var index = id.split('-');
		for(var i = 0,j = arrayVal.length; i < j; i++){
			if(arrayVal[i].id == index[1]){
				arrayVal[i].notes = updateNote;
				arrayVal[i].head = updateTitle;	
			}
		}
		setItemsToLocalStorage("Note_Objects",arrayVal);
}

//simply used to fetch data  from localstorage and invoke function to display it.
function fetchFromLocalStorage(varName){
	var localArray = JSON.parse(localStorage.getItem(varName));
	for(var i = 0,j = localArray.length; i < j; i++){
		displayNotes(localArray[i]);
	}
}

//used to set values into localStorage
function setItemsToLocalStorage(varName,arrayOfObject){
	localStorage.setItem(varName,JSON.stringify(arrayOfObject));
}

//function used because JSON will not have inherited features.
function flatStringify(x) {
    for(var i in x) {
        if(!x.hasOwnProperty(i)) {
            // weird as it might seem, this actually does the trick! - adds parent property to self
            x[i] = x[i];
        }
    }
    return x;
}

//Used to process input Title and note.
function storeInputData(){
    var title =document.getElementById("title-div").innerHTML;
    var textData = document.getElementById("note-div").innerHTML;
    if(!title && !textData){
    	alert("Enter some Notes");
    }else {
     document.getElementById("input-div").style.display = "block";
     document.getElementById("input-hide-div").style.display = "none";
     CREATE_DIV = true;
     createNewNote(title,textData);
     HIDEBAR_DIV = false;
 	}
 	document.getElementById("title-div").innerHTML = "";
    document.getElementById("note-div").innerHTML = "";
}

//used to create new div and invoke the functio to display it.
function createNewNote(Title,textData){
	var arrayVal = JSON.parse(localStorage.getItem("Note_Objects"));
	for(var i = 0,j = arrayVal.length; i < j; i++);
	var counter = arrayVal[--i].id;
	var newNoteContainer = new uniqueNoteContainerFeatures(++counter);
	newNoteContainer.head = Title;
	newNoteContainer.notes = textData;
	displayNotes(newNoteContainer);
}

//used to close selected container
function closeContainer(){
	event.stopPropagation();
    var containerNode = event.target.parentNode.parentNode;
    var tempVal = JSON.parse(localStorage.getItem("Note_Objects"));
    for(var x = 0,y = tempVal.length; x < y ; x++){
    	console.log(tempVal[x].id);
    	if(tempVal[x].id == containerNode.id.split('-')[1]){
    		tempVal.splice(x,1);
    		break;
    	}
    }
    setItemsToLocalStorage("Note_Objects",tempVal);
    containerNode.parentNode.removeChild(containerNode);
}


//used to select container
function selectContainer(){
	event.stopPropagation();
	var hideHedder = document.getElementsByClassName("hide-site-hedder")[0];
	var visibleHedder = document.getElementsByClassName("visible-site-hedder")[0];
	var containerNode = event.target.parentNode.parentNode;
	var index = containerNode.id.split('-')[1];
	if(ticked[index] == undefined){
		ticked[index] = true;  
		hideHedder.style.display = "block";
		visibleHedder.style.display = "none";
		containerNode.classList.add("select-container");
	}else if(ticked[index] == true){
		ticked[index] = undefined;
		if(ticked.indexOf(true) == -1){
			hideHedder.style.display = "none";
			visibleHedder.style.display = "block";
		}
		containerNode.classList.remove("select-container");
	}
}

function changeColor(){
	event.stopPropagation();
	var colorId = event.target.id.split('-')[1];
	//console.log(colorId);
	var note = document.getElementById(event.target.parentNode.parentNode.parentNode.id);
	var colors = {
		'gal1' : function(){
				 note.style.background = "#D2691E";
				},
		'gal2' : function(){
				  note.style.background = "red";
				},
		'gal3' : function(){
				  note.style.background = "orange";
				},
		'gal4' : function(){
				  note.style.background = "#00FFFF";
				},
		'gal5' : function(){
				  note.style.background = "blue";
				},
		'gal6' : function(){
				  note.style.background = "green";
				}

	};
	 colors[colorId]();
}





